export default class LocalStorageManager {

    entries = []

    constructor (key, updateFunction) {
        this.key = key
        this.watcher = updateFunction
    }

    entriesUpdate () {
        this.watcher(this.entries)
    }

    getAllEntries () {
        const tmpEntries = JSON.parse(localStorage.getItem(this.key))
        this.entries = tmpEntries ? tmpEntries : []
        this.entriesUpdate()
    }

    addEntry (item) {
        item.id = `b_${item.createdAt}_${this.entries.length}`
        this.entries.push(item)
        localStorage.setItem(this.key, JSON.stringify(this.entries))
        this.entriesUpdate()
    }

    deleteEntry (itemId) {
        this.entries = this.entries.filter(e => e.id !== itemId)
        localStorage.setItem(this.key, JSON.stringify(this.entries))
        this.entriesUpdate()
    }

    updateEntry (itemId, newValue) {
        for (let i = 0; i < this.entries.length; i++) {
            if (this.entries[i].id === itemId) {
                this.entries[i].url = newValue
                this.entries[i].updatedAt = new Date().getTime()
                break
            }
        }

        localStorage.setItem(this.key, JSON.stringify(this.entries))
        this.entriesUpdate()
    }

}