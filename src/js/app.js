import {
  validateUrl
} from './validateUrl'
import LocalStorageManager from "./localStorageManager";
import Router from './router'
import BookmarksList from './bookmarksList'

export default class App {

  bookmarks = [];

  constructor(rootElementId) {
    const rootElement = document.getElementById(rootElementId)
    this.router = new Router(rootElementId)
    this.bookmarksManager = new LocalStorageManager("bookmarks", this.bookmarksUpdate.bind(this))
    this.table = new BookmarksList(rootElementId, this.bookmarks, 20)
    this.attachListeners(rootElement)
    this.bookmarksManager.getAllEntries()
  }

  bookmarksUpdate (bookmarks) {
    this.bookmarks = bookmarks
    this.table.updateBookmarks(this.bookmarks)
  }

  attachListeners (element) {
    element.addEventListener("click", event => {
      event.stopPropagation()
      this.processEvent(event)
    })
    window.addEventListener("rerender", () => this.render())
  }

  processEvent (event) {
    const eventId = event.path[0].getAttribute('data-event-type')

    switch (eventId) {
      case "add-url":
        this.handleUrlSubmission();
        break;
      case "delete-bookmark":
        this.handleDeleteBookmark(event.path[0].getAttribute('data-bookmark-id'))
        break;
      case "update-bookmark":
        this.handleUpdateBookmark(event.path[0].getAttribute('data-bookmark-id'))
        break;

      default:
        console.warn(`Unknown event ID "${eventId}"`);
    }
  }


  async handleUrlSubmission () {
    const url = document.getElementById("url-input").value;
    const {valid, errorMessage} = await validateUrl(url)

    if (valid) {
      this.addUrl(url)
    } else {
      this.displayErrorMessage(errorMessage)
    }
  }

  handleDeleteBookmark (id) {
    this.bookmarksManager.deleteEntry(id)
  }

  async handleUpdateBookmark (id) {
    const newValue = document.querySelector(`#${id} input[type="url"]`).value
    const {valid, errorMessage} = await validateUrl(newValue)

    if (valid) {
      this.bookmarksManager.updateEntry(id, newValue)
      document.getElementById(id).classList.remove('bookmark-item--update-error')
    } else {
      document.getElementById(id).classList.add('bookmark-item--update-error')
      document.querySelector("#"+id + " .bookmark-item__update-message").innerText = errorMessage
    }
  }

  addUrl (url) {
    this.bookmarksManager.addEntry({url, createdAt: new Date().getTime()})
    this.router.navigate('/success')
  }

  displayErrorMessage (message) {
    document.getElementById("message-area").className += ' error'
    document.getElementById("message-area").innerHTML = `
      <span class="error-message" id="error-message">${message}</span>
    `
  }

  render () {
    this.table.renderTable()
  }

}
