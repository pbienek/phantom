const home = () => {

return `
    <h1>Home Page</h1>
    <div id="input-area">
      <label for="url-input">Enter Url</label>
      <input id="url-input" type="url" placeholder="eg. phantom.land"  required value="" />
      <button type="button" data-event-type="add-url">Add Url</button>
      <div id="message-area"></div>
    </div>
    
    
    <div>
        <ul id="bookmark-list"></ul>
        <div id="bookmark-pagination" class="pagination"></div>
    </div>`
}

export default home