const successPage = () => {
    const bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    const addedBookmark = bookmarks.pop()
    return `
        <h1>Thank you for your submission</h1>
        <div>You added <strong>${addedBookmark.url}</strong> to your list</div>
        <a id="back-to-home" href="/" data-event-type="link-click">Back to Home</a>
    `
}

export default successPage