import moment from 'moment'

class BookmarksList {
    currentPage =  1
    totalPages = 1
    tableId = 'bookmark-list'

    constructor (rootId, bookmarks, pageSize) {
        this.pageSize = pageSize
        this.attachListener(rootId)
    }

    attachListener (rootId) {
        document.getElementById(rootId).addEventListener("click", event => {
            if (event.path[0].getAttribute('data-event-type') === 'pagination-action') {
                event.stopPropagation()
                this.currentPage = event.path[0].getAttribute('data-page')
                this.renderTable()
            }

            if (event.path[0].getAttribute('data-event-type') === 'edit-bookmark') {
                event.stopPropagation()
                event.path[2].classList.toggle("bookmark-item--edit-mode")
            }
        })
    }

    updateBookmarks (bookmarks) {
        this.bookmarks = bookmarks
        this.renderTable()
    }

    renderTable () {

        const tableElement = document.getElementById(this.tableId)
        if (!tableElement) return

        this.totalPages = Math.ceil(this.bookmarks.length / this.pageSize);
        const currentPage = Math.min(this.currentPage, this.totalPages);
        const startBookmark = (currentPage - 1) * this.pageSize;
        const displayedBookmarks = this.bookmarks.slice(startBookmark, startBookmark + this.pageSize);

        tableElement.innerHTML = ''

        displayedBookmarks.forEach(bookmark => {
            const itemHtml = `
                <li id="${bookmark.id}" class="bookmark-item">
                    <div class="bookmark-item__label">
                        <span class="bookmark-item__label-date">${moment(bookmark.createdAt).format('lll')}</span>
                        <span class="bookmark-item__label-url">${bookmark.url}</span>
                    </div>
                    <div class="bookmark-item__edit-area">  
                        <input type="url" value="${bookmark.url}" />
                        <button data-event-type="update-bookmark" data-bookmark-id="${bookmark.id}">Update</button>
                        <div class="bookmark-item__update-message"></div>
                    </div>
                    <div class="bookmark-item__actions">
                         <button class="bookmark-item__button" data-event-type="edit-bookmark" data-bookmark-id="${bookmark.id}">Edit</button>
                        <button class="bookmark-item__button bookmark-item__button--delete" data-event-type="delete-bookmark" data-bookmark-id="${bookmark.id}">Delete</button>
                    </div>
                </li>
                `
            tableElement.insertAdjacentHTML('beforeend', itemHtml)
        })

        this.renderPagination(currentPage, this.totalPages)
    }

    renderPagination (currentPage, totalPages) {
        const paginationElement = document.getElementById('bookmark-pagination')
        paginationElement.innerHTML = ''

        if (currentPage >= 2) paginationElement.insertAdjacentHTML('beforeend', `
            <span class="pagination__item" data-event-type="pagination-action" data-page="${currentPage - 1}"><--</span>
        `)
        for (let i = 1; i<=totalPages; i++) {

            let classes = ["pagination__item"]
            if (i === currentPage) {
                classes.push(["pagination__item--active"])
            }

            paginationElement.insertAdjacentHTML('beforeend', `
                <span class="${classes.join(' ')}" data-event-type="pagination-action" data-page="${i}">${i}</span>
            `)
        }
        if (currentPage !== totalPages && totalPages > 1) paginationElement.insertAdjacentHTML('beforeend', `
            <span class="pagination__item" data-event-type="pagination-action" data-page="${currentPage + 1}">--></span>
        `)
    }
}
export default BookmarksList