import homePage from "./pages/home";
import successPage from "./pages/success";

class Router {

    constructor(rootElementId) {
        this.content = document.getElementById(rootElementId);
        this.content.innerHTML = this.getRoute(window.location.pathname)
        this.rerenderEvent = new Event("rerender", { bubbles: true, cancelable: false });

        window.onpopstate = () => {
            this.content.innerHTML = this.getRoute(window.location.pathname);
            window.dispatchEvent(this.rerenderEvent);
        }

        this.addLinkListener(this.content)
    }

    navigate (path) {
        window.history.pushState({}, path,window.location.origin + path);
        this.content.innerHTML =  this.getRoute(path)
        window.dispatchEvent(this.rerenderEvent);
    }

    getRoute (route) {
        switch (route){
            case '/':
                return homePage()
            case "/success":
                return successPage()

        }
    };

    addLinkListener (element) {

        element.addEventListener("click", event => {

            if (event.path[0].getAttribute('data-event-type') === 'link-click') {
                event.stopPropagation()
                event.preventDefault()
                this.navigate(event.path[0].getAttribute('href'))
            }
        })
    }

}

export default Router