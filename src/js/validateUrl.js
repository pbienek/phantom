export const isValidUrl = (url) => {
    //Regex borrowed from http://urlregex.com/
    const regX = RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/
    )
    return regX.test(url)
}

export const doesExist = async (url) => {

    let cleanedUrl = url;
    if (!(url.indexOf("http://") === 0 || url.indexOf("https://") === 0)) {
        cleanedUrl = "http://" + url
    }

    let valid = false
    await fetch(cleanedUrl,{
        redirect: 'follow',
        method: 'GET',
        mode: 'no-cors'
    }).then(response => {
        if (response.status !== 404) valid = true
    })
    .catch((err) => {})

    return valid
}

export const validateUrl = async (url) => {

    if (!isValidUrl(url)) return {
        valid: false,
        errorMessage: url.length ? "This is not a valid URL string." : "No URL entered"
    }

    const urlBody = await doesExist(url)

    if (!urlBody) return {
        valid: false,
        errorMessage: "This URL does not exist."
    }

    return {
        valid: true,
        errorMessage: null
    }
}